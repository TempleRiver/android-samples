package com.android.sample01.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.android.sample01.R;

public class IndexActivity2 extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_index, menu);
        return true;
    }
}
