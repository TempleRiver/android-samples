import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.util.IO;

public class Main {

	//http://wiki.eclipse.org/Jetty/Tutorial/Embedding_Jetty
	public static void main(String[] args) throws Exception {
		Server server = new Server(8091);
		ContextHandler staticContext = new ContextHandler();
		staticContext.setContextPath("/static");

		ResourceHandler handler = new ResourceHandler();
		handler.setResourceBase("./src/main/webapp/");
		handler.setDirectoriesListed(true);
		staticContext.setHandler(handler);

		ContextHandler mainContext = new ContextHandler();
		mainContext.setContextPath("/");
		mainContext.setHandler(new AbstractHandler() {

			@Override
			public void handle(String target, Request baseRequest,
					HttpServletRequest request, HttpServletResponse response)
					throws IOException, ServletException {
				response.setContentType("text/html;charset=utf-8");
				response.setHeader("Pragma", "no-cache");
				response.setStatus(HttpServletResponse.SC_OK);
				baseRequest.setHandled(true);
				response.getWriter().write(IO.toString(new FileInputStream(new File("src/main/webapp/index.html")),"UTF-8"));

			}
		});


		ContextHandlerCollection collection = new ContextHandlerCollection();
		collection.addHandler(staticContext);
		collection.addHandler(mainContext);
		server.setHandler(collection);
		server.start();
		server.join();
	}

}
